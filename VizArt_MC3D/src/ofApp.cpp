#include "ofApp.h"

string surfaceTypes[] = {"noise","sine^2"};

//--------------------------------------------------------------
void ofApp::setup()
{
    ofSetFrameRate(30); // run at 60 fps
    ofSetVerticalSync(true);
    //Background colors
    colorOne.set (0,80,120);
    colorTwo.set (0,0,40);
    //Activate OpenGL
    glEnable(GL_DEPTH_TEST);
    differentSurfaces = 0;
    drawGrid = false;
    showMenu = false;
    mc.setup();
    mc.setResolution(32,16,32);
    mc.scale.set(1000, 500, 1000 );
    mc.threshold=0.03;
    mc.setSmoothing(true);
    shader.load("shaders/normalShader");
    wireframe=true;
//    if (ofIsGLProgrammableRenderer()) {
//        shader.load("shaders_gl3/normalShader");
//    }
//    else {
//        shader.load("shaders/normalShader");
//    }
    //
    for(int i = 0; i < kNumLights; i++) {
        light[i].setDirectional();
        if(i==0){
            light[i].setOrientation(ofVec3f(30,80,80));
        }else if(i==1){
            light[i].setOrientation(ofVec3f(-30,-80,80));
        }else if(i==2){
            light[i].setOrientation(ofVec3f(180,0,0));
        }else{
            light[i].setOrientation(ofVec3f(ofRandom(-180,180),ofRandom(-180,180),ofRandom(-180,180)));
        }
    }
    camDist=1000;
    //Set 3D Cameras
    for(int i = 0; i < kNumCameras; i++) {
        cam[0].setPosition(0,0,camDist*2);
        cam[0].lookAt(ofVec3f(0,0,0));
        ofSeedRandom();
        cam[i].setPosition(ofRandom(-camDist,camDist),ofRandom(-camDist,camDist),ofRandom(-camDist,camDist));
        cam[i].lookAt(ofVec3f(0,0,0));
    }
    //Start with Camera 0
    camToView = 0;
    //Time which the camera view will remain, till a new perspective will be set
    camSwapTime = 2000;
    //Set Start Time
    startTimeDraw = ofGetElapsedTimeMillis();
    startTimeSwap = ofGetElapsedTimeMillis();
    //Time one draw mode remains
    drawTime = 10000;
    //Setup OSC Receiver
    receiver.setup(PORT);
    //BeatPerMinute indicates rhytmical dynamic of changes
    bpm = 120;
    //OSC values
    val1=1;
    val2=1;
    val3=1;
    val4=1;
    val5=1;
}

//--------------------------------------------------------------
void ofApp::update()
{
    // check for waiting messages
    while(receiver.hasWaitingMessages()){
        // get the next message
        ofxOscMessage m;
        receiver.getNextMessage(m);
        cout<<m<<endl;
        // check for mouse moved message
        if(m.getAddress() == "/vizart"){
            mc.flipNormals();
            bpm = m.getArgAsInt(0);
            //towardStep = float(bpm/6000.0);
            val1 = m.getArgAsFloat(1)+ofRandom(0.8,1.0)*3.0f+0.5f;
            val2 = m.getArgAsFloat(2)+ofRandom(0.8,1.0)*3.0f+0.5f;
            val3 = ofMap(m.getArgAsFloat(3),100.0,1000.0,100.0,4000.0);
            val4 = ofMap(m.getArgAsFloat(4),0.25,16,0.2,5.0);
            val5 = m.getArgAsFloat(5)/2+ofRandomuf()*3.0f+0.5f;
        }
    }
    elapsedTime = ofGetElapsedTimef();
    //freq=ofRandom(100.0,2000.0);
    freq=val3;
    //amp=ofRandom(0.1,1.0);
    amp=val1+val2;
    //Values controlled by Frequency
    sinScale= freq/1000;
    noiseScale=sinScale/10;
    //Values controlled by Amplitude
    step = elapsedTime * amp;
    //
    //Calculating passed time
    curTimeDraw = ofGetElapsedTimeMillis() - startTimeDraw;
    curTimeSwap = ofGetElapsedTimeMillis() - startTimeSwap;
    //
    if(curTimeDraw>drawTime){
        ofSeedRandom();
        //Choose ifferent Shapes randomly
        pickSurf = ofRandom(0.0,1.0);
        //cout<<pickSurf<<endl;
        if(pickSurf >= 0.5){
            differentSurfaces = 1;
        }
        else if(pickSurf < 0.5){
            differentSurfaces = 0;
        }
        pickWire = ofRandom(0.0,1.0);
        if(pickWire >= 0.5){
            wireframe = true;
        }
        else if(pickWire < 0.5){
            wireframe = false;
        }
        drawTime = ofRandom(1000,ofRandom(2000,30000));
        startTimeDraw = ofGetElapsedTimeMillis();
    }
    //Change randomly choosen camera
    if(curTimeSwap>camSwapTime){
        ofSeedRandom();
        //Set 3D Cameras
        for(int i = 0; i < kNumCameras; i++) {
            cam[0].setPosition(0,0,camDist*2);
            cam[0].lookAt(ofVec3f(0,0,0));
            cam[i].setPosition(ofRandom(-camDist,camDist),ofRandom(-camDist,camDist),ofRandom(-camDist,camDist));
            cam[i].lookAt(ofVec3f(0,0,0));
        }
        startTimeSwap = ofGetElapsedTimeMillis();
        camSwapTime = ofRandom(200,ofRandom(1000,30000));
        //Randomly setting cam position
        camToView = ofRandom(kNumCameras-1);
        //Randomly change background color
        colorOne.set(ofRandom(20,255),ofRandom(20,255),ofRandom(20,255));
        colorTwo.set(ofRandom(20,255),ofRandom(20,255),ofRandom(20,255));
    }
    //
    if(differentSurfaces == 0){
        //NOISE
        float noiseScale2 = noiseScale * val5;
        for(int i=0; i<mc.resX; i++){
            for(int j=0; j<mc.resY; j++){
                for(int k=0; k<mc.resZ; k++){
                    //NOISE
                    float nVal = ofNoise(float(i)*noiseScale, float(j)*noiseScale, float(k)*noiseScale + step);
                    if(nVal > .0)	nVal *= ofNoise(float(i)*noiseScale2, float(j)*noiseScale2, float(k)*noiseScale2 + step);
                    mc.setIsoValue( i, j, k, nVal );
                }
            }
        }
    }
    else if(differentSurfaces == 1){
        //SIN
        for(int i=0; i<mc.resX; i++){
            for(int j=0; j<mc.resY; j++){
                for(int k=0; k<mc.resZ; k++){
                    float val = sin(float(i)*sinScale) + cos(float(j)*sinScale) + sin(float(k)*sinScale + step);
                    mc.setIsoValue( i, j, k, val * val );
                }
            }
        }
    }
    //Update the mesh
    mc.update();
}

//--------------------------------------------------------------
void ofApp::draw(){
    ofSetWindowTitle(ofToString(ofGetFrameRate()));
    ofBackgroundGradient(colorOne,colorTwo,OF_GRADIENT_LINEAR);
    ofPushMatrix();     // push the current coordinate position
    //ofRotateX(90);    // change the coordinate system
    cam[camToView].begin();
    //Draw the mesh
    shader.begin();
    //
    if(ofIsGLProgrammableRenderer()){
        //ofMatrix4x4 normalMatrix = ofMatrix4x4::getTransposedOf((ofGetCurrentMatrix(OF_MATRIX_MODELVIEW)).getInverse());
        ofMatrix4x4 normalMatrix = ofMatrix4x4::getTransposedOf((ofGetCurrentMatrix(OF_MATRIX_MODELVIEW)));
        shader.setUniformMatrix4f("uNormalMatrix", normalMatrix);
    }
    //Switch from shaders to wireframe, if actived
    if(wireframe){
        mc.drawWireframe();
    }
    else{
        mc.draw();
    }
    shader.end();
    //Draw the voxel grid
    if(drawGrid){
        mc.drawGrid();
    }
    cam[camToView].end();
    ofPopMatrix();
    //
    if(showMenu==true)
    {
        string info = "fps:" + ofToString(ofGetFrameRate()) +
        + "\nnum vertices:" + ofToString(mc.vertexCount, 0)
        + "\nthreshold:" + ofToString(mc.threshold)
        + "\n'n' flips normals "
        + "\n's' toggles smoothing"
        + "\n'w' toggles wireframe"
        + "\n'f' toggles fullscreen"
        + "\n'g' toggles draw grid"
        + "\n'p' toggles pause"
        + "\n'up/down' +- threshold"
        + "\n'm' toggles menu";
        ofDrawBitmapString(info, 20, 20);
    }
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    //Key mapping
    if(key == 'f'){
        ofToggleFullscreen();
    }
    if(key == 'w'){
        wireframe = !wireframe;
    }
    if(key == 'n'){
        mc.flipNormals();
    }
    if(key == 's'){
        mc.setSmoothing(!mc.getSmoothing() );
    }
    if(key == 'g'){
        drawGrid = !drawGrid;
    }
    if(key == 'm'){
       showMenu = !showMenu;
    }
    if(key == '+'){
       mc.threshold += .03;
    }
    if(key == '-'){
        mc.threshold -= .03;
    }
    if(key == 'c'){
        camToView = ofRandom(1,kNumCameras);
    }
    if(key == 'i'){
        camToView = 0;
    }
}
