#pragma once
#include "ofMain.h"
#include "ofxOsc.h"
#include "ofxMarchingCubes.h"
// listening port
#define PORT 12345
// camera jumper
#define kNumCameras 8
#define kNumLights 3
// max number of strings to display
#define NUM_MSG_STRINGS 20
// demonstrates receiving and processing OSC messages with an ofxOscReceiver,
// use in conjunction with the oscSenderExample
class ofApp : public ofBaseApp{
    public:
        void setup();
        void update();
        void draw();
        void keyPressed(int key);
        //Fonts
        ofTrueTypeFont font;
        //Osc messaging
        ofxOscReceiver receiver;
        float val1,val2,val3,val4,val5;
        int bpm;
        //Shape values
        float noiseScale,sinScale,step,freq,amp;
        ofVboMesh mainMesh;
        ofxMarchingCubes mc;
        ofShader shader;
        ofColor colorOne,colorTwo;
        int differentSurfaces;
        float pickSurf,pickWire;
        bool wireframe,drawGrid,showMenu;
        float extrusionAmount;
        //Cameras and Lights
        ofEasyCam cam[kNumCameras];
        ofLight light[kNumLights];
        int camDist;
        int camToView; // which camera index are we looking through
        int camSwapTime, drawTime; // which camera index we are configuring
        //Time Clock
        int startTimeDraw, curTimeDraw, startTimeSwap, curTimeSwap;
        float elapsedTime;
};
