# OSC Setup for VizArt Pieces
from pythonosc.udp_client import SimpleUDPClient
ip = "127.0.0.1"
port = 12345
client = SimpleUDPClient(ip,port)  # Create client
bpm = float(60/Clock.bpm)
def mean(numbers):
    return float(sum(numbers))/max(len(numbers),1)
# Change values of val1, val2, val3, val4, val5 to adjust other values to the visualization
def oscMsg():
    bpm = float(60/Clock.bpm) 
    val0 = int(Clock.bpm)             	#Sends current bpm (bass per minute)
    val1 = mean(Master().amp)  		#Sends mean of all amplitudes
    val2 = mean(Master().amplify)	#Sends mean of all amplifications
    val3 = mean(Master().freq)		#Sends mean of all pitches
    val4 = mean(Master().dur)		#Sends mean of all durations
    val5 = mean(Master().oct)		#Sends mean of all octaves
    client.send_message("/vizart",[val0,val1,val2,val3,val4,val5])
    Clock.future(bpm,lambda:oscMsg())
oscMsg()


