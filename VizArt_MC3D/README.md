# VizArt - Marching Cubes 3D

 This FoxDot visualization creates reactive polygonized scalar fields. It is an implementation after Paul Bourke polygonize voxel.

 http://paulbourke.net/geometry/polygonise/

# Osc setup for "VizArt" pieces

Use FoxDot_template.py:

  * Execute code in FoxDot_template.py to establish OSC connection to a VizArt piece

  * Change values of val1, val2, val3, val4, val5 to adjust other values to the visualization


## Marching Cubes 3D Options

* Press "f" to toggle fullscreen
* Press "i" to set camera to idle position
* Press "c" to picks randomly another camera position
* Press "g" to toggle grid view
* Press "n" to flip normals
* Press "s" to set smoothing
* Press "+" to increase mesh threshold by 0.3
* Press "-" to decrease mesh threshold by 0.3
* Press "m" to toggle menu

![Screenshot](screenshot1.png)
![Screenshot](screenshot2.png)

Credits to the creator of the original version:
https://github.com/Larsberg/ofxMarchingCubes
