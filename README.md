# VizArt - Visual Art for FoxDot

This project produces visualizations for LiveCoding Music environments, in particular FoxDot.

* FoxDot is a Python programming environment that provides a fast and user-friendly abstraction to SuperCollider. It also comes with its own IDE, which means it can be used straight out of the box; all you need is Python and SuperCollider and you're ready to go!

* All VizArt projects are written in C++ using openFrameworks.

* The interactivity of the graphics are controlled via OSC Messaging

* FoxDot_template.py contains the code needed to establish an OSC connection
