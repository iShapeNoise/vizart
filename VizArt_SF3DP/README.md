# VizArt - Superformula 3D - Polygony

A simple implementation of Superformula - <http://en.wikipedia.org/wiki/Superformula> is used to create this FoxDot visualization.

# Osc setup for "VizArt" pieces

Use FoxDot_template.py:

  * Execute code in FoxDot_template.py to establish OSC connection to a VizArt piece

  * Change values of val1, val2, val3, val4, val5 to adjust other values to the visualization


## Superformula 3D Polygony Options

* Press "f" to toggle fullscreen
* Press "i" to set camera to idle position
* Press "c" to picks randomly another camera position
* Press "r" to generate a new shape randomly
* Press "p" to toggle point view
* Press "w" to toggle wire view
* Press "b" to toggle point and wire view simultaneously

![Screenshot](screenshot1.png)
![Screenshot](screenshot2.png)

Credits to the creator of the original version:
https://raw.github.com/kamend/Superformula3d/
