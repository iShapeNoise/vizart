#include "ofApp.h"
#include "ofxMeshUtils.h"

//Function that makes previous shape morphs into next shape
float moveTowards(float current, float target, float step)
{
    if(abs(current-target) < step)
    {
        current = target;
    }
    else
    {
        if(current > target)
        {
            current -= step;
        } else if(current < target)
        {
            current += step;
        }
    }
    return current;
}
//
ofVec3f ofApp::sf3d(float x, float y) {
    //
    float i = -PI + x*stepvalue;
    float j = -PI/2.0 + y*stepvalue;
    //
    float raux1 = pow(abs(1/a1value*abs(cos(n1value * i/4))),n3value)+pow(abs(1/a2value*abs(sin(n1value*i/4))),n4value);
    float r1=pow(abs(raux1),(-1/n2value));
    //
    float raux2=pow(abs(1/a1value*abs(cos(n1value*j/4))),n3value)+pow(abs(1/a2value*abs(sin(n1value*j/4))),n4value);
    float r2=pow(abs(raux2),(-1/n2value));
    //
    float xx=r1*cos(i)*r2*cos(j)*100.0f;
    float yy=r1*sin(i)*r2*cos(j)*100.0f;
    float zz=r2*sin(j)*100.0f;
    //
    return ofVec3f(xx,yy,zz);
}

//--------------------------------------------------------------
void ofApp::setup(){
    //
    for(int i = 0; i < kNumLights; i++) {
        light[i].setDirectional();
        if(i==0){
            light[i].setOrientation(ofVec3f(30,80,80));
        }else if(i==1){
            light[i].setOrientation(ofVec3f(-30,-80,80));
        }else if(i==2){
            light[i].setOrientation(ofVec3f(180,0,0));
        }else{
            light[i].setOrientation(ofVec3f(ofRandom(-180,180),ofRandom(-180,180),ofRandom(-180,180)));
        }
    }
    camDist=2000;
    //Set 3D Cameras
    for(int i = 0; i < kNumCameras; i++) {
        cam[0].setPosition(0,0,camDist*2);
        cam[0].lookAt(ofVec3f(0,0,0));
        ofSeedRandom();
        cam[i].setPosition(ofRandom(-camDist,camDist),ofRandom(-camDist,camDist),ofRandom(-camDist,camDist));
        cam[i].lookAt(ofVec3f(0,0,0));
    }
    //Start with Camera 0
    camToView = 0;
    //Time which the camera view will remain, till a new perspective will be set
    camSwapTime = 2000;
    //Random generated Superformula values
    a1value = ofRandomuf()*3.0f+0.5f;
    a2value = ofRandomuf()*3.0f+0.5f;
    n1value = ofRandomuf()*20.0f;
    n2value= ofRandomuf()*3.0f+0.5f;
    n3value = ofRandomuf()*3.0f+0.5f;
    n4value = ofRandomuf()*3.0f+0.5f;
    stepvalue = ofRandom(0.02,0.9);
    //Random generated Superformula values of next shape
    a1target = ofRandomuf()*3.0f+0.5f;
    a2target = ofRandomuf()*3.0f+0.5f;
    n1target = ofRandomuf()*20.0f;
    n2target = ofRandomuf()*3.0f+0.5f;
    n3target = ofRandomuf()*3.0f+0.5f;
    n4target = ofRandomuf()*3.0f+0.5f;
    //Booleans to trigger different visual options
    drawWire = true;
    drawPoints = false;
    //Rotate Shape
    rotationZ = 0.0;
    rotationSpeed = 1.0;
    //Set Start Time
    startTimeDraw = ofGetElapsedTimeMillis();
    startTimeSwap = ofGetElapsedTimeMillis();
    //Time one draw mode remains
    drawTime = 10000;
    //Setup OSC Receiver
    receiver.setup(PORT);
    //BeatPerMinute indicates rhytmical dynamic of changes
    bpm = 120;
    towardStep = float(bpm/6000.0);
}

//--------------------------------------------------------------
void ofApp::update(){
    //Update rotation
    rotationZ = rotationZ+rotationSpeed;
    // check for waiting messages
    while(receiver.hasWaitingMessages()){
        // get the next message
        ofxOscMessage m;
        receiver.getNextMessage(m);
        cout<<m<<endl;
        // check for mouse moved message
        if(m.getAddress() == "/vizart"){
            bpm = m.getArgAsInt(0);
            towardStep = float(bpm/6000.0);
            a1target = m.getArgAsFloat(1)+ofRandom(0.8,1.0)*3.0f+0.5f;
            a2target = m.getArgAsFloat(2)+ofRandom(0.8,1.0)*3.0f+0.5f;
            n1target = m.getArgAsFloat(3)/100+ofRandomuf()*20.0f;
            n2target = m.getArgAsFloat(4)+ofRandomuf()*3.0f+0.5f;
            n3target = m.getArgAsFloat(5)/2+ofRandomuf()*3.0f+0.5f;
            n4target = m.getArgAsFloat(2)+ofRandomuf()*3.0f+0.5f;
            stepvalue = ofRandom(0.02,0.9);
        }
    }
    //Calculating passed time
    curTimeDraw = ofGetElapsedTimeMillis() - startTimeDraw;
    curTimeSwap = ofGetElapsedTimeMillis() - startTimeSwap;
    //
    if(curTimeDraw>drawTime){
        ofSeedRandom();
        //drawWire = !drawWire;
        drawPoints = !drawPoints;
        drawTime = ofRandom(1000,ofRandom(2000,30000));
        rotationSpeed = ofRandomuf();
        startTimeDraw = ofGetElapsedTimeMillis();
    }
    //Change randomly choosen camera
    if(curTimeSwap>camSwapTime){
        ofSeedRandom();
        //Set 3D Cameras
        for(int i = 0; i < kNumCameras; i++) {
            cam[0].setPosition(0,0,camDist*2);
            cam[0].lookAt(ofVec3f(0,0,0));
            cam[i].setPosition(ofRandom(-camDist,camDist),ofRandom(-camDist,camDist),ofRandom(-camDist,camDist));
            cam[i].lookAt(ofVec3f(0,0,0));
        }
        startTimeSwap = ofGetElapsedTimeMillis();
        camSwapTime = ofRandom(200,ofRandom(1000,30000));
        camToView = ofRandom(kNumCameras-1);
    }
    //Shape changes to next generated mesh in bpm speed
    a1value = moveTowards(a1value,a1target,towardStep);
    a2value = moveTowards(a2value,a2target,towardStep);
    n1value = moveTowards(n1value,n1target,towardStep);
    n2value = moveTowards(n2value,n2target,towardStep);
    n3value = moveTowards(n3value,n3target,towardStep);
    n4value = moveTowards(n4value,n4target,towardStep);
    //Generated mesh is cleared before the next mesh is created
	mesh.clear();	
    //
	int N_X = ceil((2.0*PI) / stepvalue);
	int N_Y = ceil(PI / stepvalue);
    //
	for(int x=0;x<N_X;x++) {
		for(int y=0;y<N_Y;y++) {
			mesh.addVertex(sf3d(x,y));
            if(curTimeSwap>camSwapTime){
                mesh.addColor(ofColor(ofRandom(20,255),ofRandom(20,255),ofRandom(20,255)));
            }
		}
	}
	mesh.addVertex(sf3d(PI/stepvalue,PI/stepvalue));
    mesh.addColor(ofColor(ofRandom(20,255),ofRandom(20,255),ofRandom(20,255)));
    //
	lastRow.clear();
    //
	for(int x=0;x<N_X;x++) {
		for(int y=0;y<N_Y-1;y++) {
			if(x == N_X-1) {				
				int idx1 = x*N_Y+y;
				int idx2 = x*N_Y+y+1;				
				int idx3 = y+1;
				int idx4 = y;				
				mesh.addTriangle(idx1, idx2, idx3);
				mesh.addTriangle(idx1, idx3, idx4);	
			} else {
				int idx1 = x*N_Y+y;
				int idx2 = x*N_Y+y+1;				
				int idx3 = (x+1)*N_Y+y+1;
				int idx4 = (x+1)*N_Y+y;				
				mesh.addTriangle(idx1, idx2, idx3);
				mesh.addTriangle(idx1, idx3, idx4);				
				if(y == N_Y-2) {
					lastRow.push_back(idx2);
				}
			}
		}
	}	
	int lastVertex = mesh.getNumVertices()-1;
    //
    for(int i=0; i < lastRow.size()-1.0; i++) {
		mesh.addTriangle(lastRow[i], lastRow[i+1], lastVertex);
	}
    mesh.addTriangle(lastRow[0], lastRow[lastRow.size()-1], lastVertex);
    //
	ofxMeshUtils::calcNormals(mesh);
}

//--------------------------------------------------------------
void ofApp::draw(){
    //
	ofBackground(0);
    //Activate OpenGL shading mode
	glShadeModel(GL_FLAT);
	glProvokingVertex(GL_FIRST_VERTEX_CONVENTION);
    //Take camera perspective set via camToView
    cam[camToView].begin();
	ofPushMatrix();
    //Rotate
    ofRotateZRad(ofDegToRad(rotationZ));
    //
	if(!drawPoints) {
		ofDisableAlphaBlending();
		mesh.setMode(OF_PRIMITIVE_TRIANGLES);
		glEnable(GL_DEPTH_TEST);
        ofEnableLighting();
        for(int i = 0; i < kNumLights; i++) {
            light[i].enable();
        }
		mesh.draw();
        for(int i = 0; i < kNumLights; i++) {
            light[i].disable();
        }
        ofDisableLighting();
    }else{
		mesh.setMode(OF_PRIMITIVE_POINTS);
		glEnable(GL_POINT_SMOOTH);
		glEnable(GL_PROGRAM_POINT_SIZE_ARB);
		glPointSize(0.5f);
		mesh.clearColors();
        if(curTimeSwap > camSwapTime){
            ofSetColor(ofRandom(20,255),ofRandom(20,255),ofRandom(20,255),100);
        }
		ofEnableAlphaBlending();
		ofEnableBlendMode(OF_BLENDMODE_ALPHA);
		mesh.draw();
	}
	if(drawWire) {
		mesh.setMode(OF_PRIMITIVE_TRIANGLES);
		mesh.clearColors();
        if(curTimeSwap > camSwapTime){
            ofSetColor(ofRandom(20,255),ofRandom(20,255),ofRandom(20,255),100);
        }
		mesh.drawWireframe();
	}
    ofPopMatrix();
	glDisable(GL_DEPTH_TEST);
    cam[camToView].end();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    if(key == 'r') {
        a1target = ofRandomuf()*3.0f+0.5f;
        a2target = ofRandomuf()*3.0f+0.5f;
		n1target = ofRandomuf()*20.0f;
		n2target = ofRandomuf()*3.0f+0.5f;
		n3target = ofRandomuf()*3.0f+0.5f;
		n4target = ofRandomuf()*3.0f+0.5f;
        stepvalue = ofRandom(0.02,0.9);
	}
	if(key == 'p') {
		drawPoints = !drawPoints;
	}
    if(key == 'w') {
		drawWire = !drawWire;
	}
    if(key == 'b'){
        if(drawPoints == false){
            drawPoints = true;
        }
        else if(drawWire == false){
            drawWire = true;
        }else{
            drawWire = !drawWire;
            drawPoints = !drawPoints;
        }
    }
	if(key == '1') {
		a1value = 0.5;
	}
	if(key == '2') {
		a2value = 0.5;
	}
	if(key == '3') {
		n1value = 0.5;
	}
	if(key == '4') {
		n2value = 0.5;
	}
	if(key == '5') {
		n3value = 0.5;
	}
	if(key == '6') {
		n4value = 0.5;
	}
    if(key == 'c'){
        camToView = ofRandom(1,kNumCameras);
    }
    if(key == 'i'){
        camToView = 0;
    }
    if(key == 'f'){
        ofToggleFullscreen();
    }
}
