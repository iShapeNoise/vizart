#pragma once

#include "ofMain.h"
#include "ofxOsc.h"

// listening port
#define PORT 12345
// camera jumper
#define kNumCameras 8
#define kNumLights 3

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();
        void keyPressed(int key);

    //OSC Messaging
    ofxOscReceiver receiver;
    float n1target,n2target,n3target,n4target,a1target,a2target,stepvalue;
    float n1value,n2value,n3value,n4value,a1value,a2value,towardStep;
    int bpm;

    //3D Mesh
    ofMesh mesh;
	ofVec3f sf3d(float x, float y);
    vector<int> lastRow;
    bool drawPoints, drawWire;
    ofParameter<float> rotateZ;
    float rotationZ, rotationSpeed;
    int drawTime;
    //GL Camera
    ofEasyCam cam[kNumCameras];
    ofLight light[kNumLights];
    int camDist;
    int camToView; // which camera index are we looking through
    int camSwapTime; // which camera index we are configuring
    //Time Clock
    int startTimeDraw, curTimeDraw, startTimeSwap, curTimeSwap;
};
